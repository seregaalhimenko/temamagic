import sqlite3
from time import time


def add_request(user_tg_id, username, phone, text):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    dt = time()
    cur.execute('INSERT INTO customer_request('
                ' user_tg_id,'
                ' username,'
                ' phone,'
                ' datetime,'
                ' text'
                ') VALUES(?, ?, ?, ?, ?)',
                (user_tg_id, username, phone, dt, text))
    con.commit()
    con.close()


def add_admin(user_tg_id):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    cur.execute('INSERT INTO admin('
                ' user_tg_id'
                ') VALUES(?)',
                (user_tg_id,))
    con.commit()
    con.close()


def get_admin(user_tg_id):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    out = cur.execute('select id from admin where user_tg_id=?', (user_tg_id,)).fetchone()
    con.commit()
    con.close()
    return out


def user_requests(quantity=None):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    if quantity:
        arg = (
            "select * from customer_request where state='not_reviewed' limit ?",
            (quantity,)
        )
    else:
        arg = ("select * from customer_request where state='not_reviewed'",)
    out = cur.execute(*arg).fetchall()
    con.commit()
    con.close()
    return out


def request_get(id: int):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    out = cur.execute("select * from customer_request where id=?", (id,)).fetchall()
    con.commit()
    con.close()
    return out


def change_state(id_customer_request, state):
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    if cur.execute('SELECT * FROM customer_request WHERE id=?', (id_customer_request,)).fetchone():
        cur.execute('UPDATE customer_request SET state =  ? where  id=?', (state, id_customer_request,))
        con.commit()
        con.close()

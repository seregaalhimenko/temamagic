import sqlite3


def create_database():
    con = sqlite3.connect("tgBot.dp")
    cur = con.cursor()
    cur.execute(
        "CREATE TABLE admin("
            "id integer PRIMARY KEY,"
            "user_tg_id integer"
        ");"
    )
    con.commit()
    cur.execute(
        "CREATE TABLE customer_request("
            "id integer PRIMARY KEY,"
            "state text default 'not_reviewed',"
            "user_tg_id integer,"
            "username text,"
            "phone text(12),"
            "datetime REAL,"
            "category integer,"
            "text text"
        ");"
    )
    con.commit()
    con.close()


if __name__ == "__main__":
    create_database()
    print("OK")

from typing import Union
from loader import dp, bot
from aiogram import types
from aiogram.utils.exceptions import BotBlocked
from aiogram.dispatcher import FSMContext
from states.states import Request, Cause, CreateAdmin
from keyboards.default import contact, create_menu
from bd import *
from time import ctime
from keyboards.inline import create_req_keyboard, close_button
from keyboards.inline.callback_data import state_callback
from keyboards.inline.callback_data import req_all_callback, back_callback, phone_confirmation_callback_data
from keyboards.inline import create_inline_list_requests, phone_confirmation


@dp.message_handler(commands="start")
async def send_welcome(message: types.Message):
    menu = create_menu(get_admin(message.from_user.id))
    await message.answer("Привет, чтобы узнать, что я могу, напиши /help.", reply_markup=menu)


@dp.message_handler(commands="menu")
async def send_menu(message: types.Message):
    menu = create_menu(get_admin(message.from_user.id))
    await message.reply("Вот меню", reply_markup=menu)


@dp.message_handler(commands="help")
async def send_help(message: types.Message):
    command = "Команды которыми вы можете воспользоваться:\n\n" \
              "/help Получить список команд доступный вам.\n\n" \
              "/menu Показать меню команд.\n\n" \
              "/create_request, /cr Создать заявку.\n\n"
    if get_admin(message.from_user.id):
        command = command + "/add_admin Добавить админа.\n\n" \
                            "(можете задать id пользователя через пробел).\n\n" \
                            "/show_requests, /sr  Показать список заявок в виде сообщения с кнопками выбора " \
                            "(можете добавить количество заявок через пробел).\n\n" \
                            "/show_requests_l, /srl Показать список заявок в виде inline кнопок " \
                            "(можете добавить  количество заявок через пробел).\n\n"
    await message.answer(command)


@dp.message_handler(commands=["add_admin", "aa"], state=None)
async def admin_add(message: types.Message):
    if not get_admin(message.from_user.id):
        await message.answer("Ты не админ, каммон!🤬")
        return
    list_msg = message.text.split(" ")
    if list_msg.__len__() == 2:
        if get_admin(list_msg[1]):
            await message.answer("Ой, а он и так админ😐")
            return
        add_admin(list_msg[1])
        await message.answer("Пользователь с id {} теперь админ🤴".format(list_msg[1]))
        return
    await message.answer("Скиньте контакт пользователя который будет администратором", reply_markup=close_button)
    await CreateAdmin.Q1.set()


@dp.message_handler(content_types=["contact"], state=CreateAdmin.Q1)
async def admin_add_q1(message: types.Message, state: FSMContext):
    if get_admin(message.contact.user_id):
        await message.answer("Ой, а он и так админ😐")
        await state.finish()
        return
    add_admin(message.contact.user_id)
    await message.answer("🤴Пользователь стал админом🤴")
    await state.finish()


@dp.message_handler(commands=["show_requests", "sr"])
async def show_requests(message: types.Message):
    if not get_admin(message.from_user.id):
        await message.answer("Ты не админ, эй!🤬")
        return
    list_msg = message.text.split(" ")
    if not len(list_msg) == 1 and (list_msg.__len__() > 2 or not list_msg[1].isdigit()):
        await message.reply("💩")
        return
    data = user_requests(list_msg[1]) if len(list_msg) == 2 else user_requests()
    for row in data:
        keyboard = create_req_keyboard(id_customer_request=row[0], user_tg_id=row[2])
        await message.answer(
            "{}\nid_user:{}\n🧔🏻username:{}\n📲phone:{}\n {} ".format(
                ctime(row[5]),
                row[2],
                row[3],
                row[4],
                row[6]
            ),
            reply_markup=keyboard
        )
    return


@dp.callback_query_handler(back_callback.filter(prefix="show_requests_buttons"))
@dp.message_handler(commands=["show_requests_l", "srl"], )
async def show_requests_buttons(message: Union[types.Message, types.CallbackQuery], callback_data: dict = None):
    if not get_admin(message.from_user.id):
        await message.answer("Ты не админ, эй! 🤬")
        return
    if isinstance(message, types.Message):
        list_req = message.text.split(" ")
        if not len(list_req) == 1 and (list_req.__len__() > 2 or not list_req[1].isdigit()):
            await message.reply("💩")
            return
        data = user_requests(list_req[1]) if len(list_req) == 2 else user_requests()
        kb = create_inline_list_requests(data)
        await message.answer('⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇⬇', reply_markup=kb)
        return
    call = message
    list_req = callback_data.get("data")
    data = user_requests(list_req)
    kb = create_inline_list_requests(data)
    await call.message.edit_text('О! Ты опять тут😄', reply_markup=kb)


@dp.callback_query_handler(req_all_callback.filter(prefix="all_data_row"))
async def show_one_request_from_list(call: types.CallbackQuery, callback_data: dict):
    req = request_get(callback_data.get("id_req"))[0]
    keyboard = create_req_keyboard(id_customer_request=req[0], user_tg_id=req[2], call=call)
    await call.message.edit_text("{}\n\n🧔🏻username: {} 📲({})\n\n{} ".format(
        ctime(req[5]),
        req[3],
        req[4],
        req[6]
    ),
        reply_markup=keyboard
    )


@dp.callback_query_handler(state_callback.filter(state="rejected"), state=None)
@dp.callback_query_handler(state_callback.filter(state="accepted"), state=None)
async def change_state_request(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    if callback_data.get("state") == "accepted":
        await call.message.delete()
        change_state(callback_data.get("id_customer_request"), callback_data.get("state"))
        await call.answer("👍Вы подтвердили заявку👍", show_alert=False)
        return
    await call.message.answer("🖍Напишите причину отклонения заявки🖍")
    await call.message.delete()
    await Cause.Q1.set()
    await state.update_data(
        {
            "id_customer_request": callback_data.get("id_customer_request"),
            "req": call.message.text,
            "user_tg_id": callback_data.get("user_tg_id")
        }
    )


@dp.message_handler(state=Cause.Q1)
async def create_cause(message: types.Message, state: FSMContext):
    answer = message.text
    data = await state.get_data()
    try:
        await bot.send_message(
            data["user_tg_id"], "Заявка\n\n" + data["req"] + "\n\nБыла отклонена по причине: " + answer
        )
        await message.answer("✉️Причина отклонения отправлена создателю заявки!✉️")
    except BotBlocked:
        await message.answer("До создателя заявки причина не дошла (((")
    change_state(data["id_customer_request"], "rejected")
    await state.finish()


@dp.callback_query_handler(text="close", state=CreateAdmin.Q1)
@dp.callback_query_handler(text="close", state=Request.Q2)
@dp.callback_query_handler(text="close", state=Request.Q1)
@dp.callback_query_handler(text="close")
async def close_message(call: Union[types.CallbackQuery, types.Message], state: FSMContext):
    if state:
        await state.finish()
    if isinstance(call, types.Message):
        message = call
        await message.delete()
        return
    await call.message.delete()


@dp.message_handler(commands=["create_request", "cr"], state=None)
async def create_request_start(message: types.Message):
    await Request.Q1.set()
    state = dp.get_current().current_state()
    msg = await message.answer("🖍Введи текст который будет в заявке🖍", reply_markup=close_button)
    await state.update_data(
        {'message_id': msg.message_id, 'chat_id': msg.chat.id}
    )


@dp.message_handler(state=Request.Q1)
async def confirmation_information(message: types.Message, state: FSMContext):
    answer = message.text
    data = await state.get_data()
    await bot.delete_message(data["chat_id"], data["message_id"])
    msg = await message.answer("📲Нужно подтвердить номер телефона📲", reply_markup=phone_confirmation)
    await state.update_data(
        {'text': answer, 'message_id': msg.message_id, 'chat_id': msg.chat.id}
    )


@dp.callback_query_handler(phone_confirmation_callback_data.filter(status="true"), state=Request.Q1)
async def confirm_contact(call: types.CallbackQuery):
    await Request.next()
    await call.message.delete()
    await call.message.answer("⬇Жмякай кнопку⬇", reply_markup=contact)


@dp.message_handler(content_types=["contact"], state=Request.Q2)
async def create_request_end(message: types.Message, state: FSMContext):
    if message.contact.user_id != message.from_user.id:
        await message.answer('Вы уверены, что это ваш телефон?🤬 '
                             'Попробуйте снова(но теперь честно)')
        return
    data = await state.get_data()
    add_request(
        user_tg_id=message.from_user.id,
        username=message.from_user.username,
        phone=message.contact.phone_number,
        text=data["text"]
    )
    await message.answer("Заявка оформлена 🤝", reply_markup=create_menu(message.from_user.id))
    await state.finish()


@dp.message_handler()
async def echo(message: types.Message):
    menu = create_menu(get_admin(message.from_user.id))
    await message.answer("Что-то я не понял! 🤷‍♂️", reply_markup=menu)

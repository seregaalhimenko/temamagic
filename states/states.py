from aiogram.dispatcher.filters.state import StatesGroup, State


class Request(StatesGroup):
    Q1 = State()
    Q2 = State()


class Cause(StatesGroup):
    Q1 = State()


class CreateAdmin(StatesGroup):
    Q1 = State()


class Menu(StatesGroup):
    Q1 = State()
    Q2 = State()
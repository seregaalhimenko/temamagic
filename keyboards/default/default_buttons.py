from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


def create_menu(admin):
    keyboard = ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    keyboard.insert(KeyboardButton(text="/help"))
    keyboard.insert(KeyboardButton(text="/create_request"))
    if admin:
        keyboard.insert(KeyboardButton(text="/add_admin"))
        keyboard.insert(KeyboardButton(text="/show_requests"))
        keyboard.insert(KeyboardButton(text="/show_requests_l"))
    return keyboard


contact = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="Отправить телефон", request_contact=True)
        ],
    ],
    resize_keyboard=True,

)

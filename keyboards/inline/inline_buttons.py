from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from keyboards.inline.callback_data import state_callback, back_callback, phone_confirmation_callback_data
from aiogram import types


def create_req_keyboard(id_customer_request: str, user_tg_id: str, call=None):
    keyboard = InlineKeyboardMarkup(row_width=2)
    acc_button = InlineKeyboardButton(
        "Принять ✅",
        callback_data=state_callback.new(
            id_customer_request=id_customer_request,
            user_tg_id=user_tg_id,
            state="accepted"
        )
    )
    keyboard.insert(acc_button)
    rej_button = InlineKeyboardButton(
        "Отклонить ❌",
        callback_data=state_callback.new(
            id_customer_request=id_customer_request,
            user_tg_id=user_tg_id,
            state="rejected"
        )
    )
    keyboard.insert(rej_button)
    if isinstance(call, types.CallbackQuery):
        keyboard.insert(InlineKeyboardButton(text="Назад ⬅", callback_data=back_callback.new(
            prefix="show_requests_buttons",
            data=str(len(call.message.reply_markup["inline_keyboard"]) - 1)
        )))
    keyboard.insert(InlineKeyboardButton(text="Закрыть ", callback_data="close"))
    return keyboard


def create_inline_list_requests(data):
    from keyboards.inline.callback_data import req_all_callback
    keyboard = InlineKeyboardMarkup(row_width=1)
    for row in data:
        button = InlineKeyboardButton(
            "{}: {}".format(row[3], row[6]),
            callback_data=req_all_callback.new(
                prefix="all_data_row",
                id_req=row[0],
            )
        )
        keyboard.insert(button)
    keyboard.insert(InlineKeyboardButton(text="Закрыть ❌", callback_data="close"))
    return keyboard


phone_confirmation = InlineKeyboardMarkup(
    inline_keyboard=[
        [InlineKeyboardButton(text="Да, меня все устраивает 👍",
                              callback_data=phone_confirmation_callback_data.new(status="true"))],
        [InlineKeyboardButton(text="Закрыть ❌", callback_data="close")]
    ]
)

close_button = InlineKeyboardMarkup()
close_button.insert(InlineKeyboardButton(text="Закрыть ❌", callback_data="close"))

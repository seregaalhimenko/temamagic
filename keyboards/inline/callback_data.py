from aiogram.utils.callback_data import CallbackData

state_callback: CallbackData = CallbackData("change_state", "id_customer_request", "user_tg_id", "state")
req_all_callback: CallbackData = CallbackData("all_data_row", "prefix", "id_req")
back_callback: CallbackData = CallbackData("back_callback", "prefix", "data")
phone_confirmation_callback_data: CallbackData = CallbackData("phone_confirmation_callback_data", "status")

